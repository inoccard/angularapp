﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngularApp2.Data.Dao;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AngularApp2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };
        private readonly CatDao catDao;
        private readonly WeatherForecastDao weatherForecastDao;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, CatDao catDao, WeatherForecastDao weatherForecastDao)
        {
            _logger = logger;
            this.catDao = catDao;
            this.weatherForecastDao = weatherForecastDao;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var wf = weatherForecastDao.Query().ToArray();
            return wf;
        }

        [HttpPost]
        public IActionResult Save()
        {
            try
            {
                var wf = weatherForecastDao.Save(new WeatherForecast
                {
                    Date = DateTime.Now,
                    TemperatureC = 2,
                    TemperatureF = 3,
                    Summary = "Outro teste"
                });
                return Ok(wf);
            }
            catch (Exception e)
            {
                throw new Exception($"Error: {e.Message}");
            }
        }
    }
}
