﻿using FluentNHibernate.Mapping;

namespace AngularApp2.Data.Mapping
{
    public class WeatherForecastMap : ClassMap<WeatherForecast>
    {
        public WeatherForecastMap()
        {
            LazyLoad();
            Id(p=>p.Id).Column("Id").GeneratedBy.Guid();
            Map(p => p.Date, "Date").Not.Nullable();
            Map(p => p.TemperatureC, "TemperatureC").Not.Nullable();
            Map(p => p.TemperatureF, "TemperatureF").Not.Nullable();
            Map(p => p.Summary, "Summary").Not.Nullable();
            Table("WeatherForecast");
        }
    }
}