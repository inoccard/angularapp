﻿using AngularApp2.Entities;
using FluentNHibernate.Mapping;

namespace AngularApp2.Data.Mapping
{
    /// <summary>
    /// Mapeamento da Tabela Cat
    /// </summary>
    public class CatMap : ClassMap<Cat>
    {
        public CatMap()
        {
            LazyLoad();
            Id(p => p.Id).Column("Id").GeneratedBy.Assigned();
            Map(p => p.Name, "Name").Nullable();
            Map(p => p.Sex, "Sex").Nullable();
            Map(p => p.Weight, "Weight").Nullable();
            Table("cat");
        }
    }
}
