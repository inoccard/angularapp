﻿using AngularApp2.Data.Mapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace AngularApp2.Data
{
    /// <summary>
    /// Responsável por gerenciar a conexão ao banco de dados
    /// </summary>
    public static class SessionFactory
    {
        private const string ConnectionString = "Server=localhost;Port=3306;Database=angularapp;Uid=root;Pwd=;";
        private static ISessionFactory session;

        public static ISessionFactory CreateSession()
        {
            if (session != null)
            {
                return session;
            }

            IPersistenceConfigurer dbConfig = MySQLConfiguration.Standard.ConnectionString(ConnectionString);


            var mapConfig = Fluently.Configure().Database(dbConfig).Mappings(c => c.FluentMappings.AddFromAssemblyOf<CatMap>());

            session = mapConfig.BuildSessionFactory();

            return session;
        }

        public static ISession StartSession() => CreateSession().OpenSession();
    }
}
