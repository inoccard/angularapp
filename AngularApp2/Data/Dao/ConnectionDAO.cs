﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AngularApp2.Data
{
    public class ConnectionDAO<TEntity> : IConnectionDAO<TEntity> where TEntity : class
    {
        public TEntity Save(TEntity entity)
        {
            using ISession session = SessionFactory.StartSession();
            using ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(entity);
                transaction.Commit();
                return entity;
            }
            catch (Exception ex)
            {
                if (!transaction.WasCommitted)
                {
                    transaction.Rollback();
                }
                throw new Exception("Erro ao inserir entity: " + ex.Message);
            }
        }

        public TEntity Update(TEntity entity)
        {
            using ISession session = SessionFactory.StartSession();
            using ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Update(entity);
                return entity;
            }
            catch (Exception ex)
            {
                if (!transaction.WasCommitted)
                {
                    transaction.Rollback();
                }
                throw new Exception("Erro ao atualizar  entity: " + ex.Message);
            }
        }

        public void Delete(TEntity entity)
        {
            using ISession session = SessionFactory.StartSession();
            using ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Delete(entity);
            }
            catch (Exception ex)
            {
                if (!transaction.WasCommitted)
                {
                    transaction.Rollback();
                }
                throw new Exception("Erro ao excluir entity: " + ex.Message);
            }
        }

        public TEntity ReturnById(int id)
        {
            using ISession session = SessionFactory.StartSession();
            return session.Get<TEntity>(id);
        }

        public IList<TEntity> Query()
        {
            using ISession session = SessionFactory.StartSession();
            var data = (from e in session.Query<TEntity>() select e).ToList();
            return data;
        }
    }
}
