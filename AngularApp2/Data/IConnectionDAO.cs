﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularApp2.Data
{
    public interface IConnectionDAO<TEntity>
    {
        TEntity Save(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        TEntity ReturnById(int id);
        IList<TEntity> Query();
    }
}
