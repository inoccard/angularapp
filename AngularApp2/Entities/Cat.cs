﻿namespace AngularApp2.Entities
{
    public class Cat
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Sex { get; set; }

        public virtual string Weight { get; set; }
    }
}
